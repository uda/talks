import os
from pathlib import Path
from argparse import ArgumentParser

__all__ = ('main',)


def write_hello_world(args):
    cwd = Path(os.getenv('PWD'))
    file_path = cwd.joinpath(args.file)
    if file_path.exists():
        write_to_existing_file(file_path, dry_run=args.dry_run)
    else:
        write_to_new_file(file_path, dry_run=args.dry_run)


def write_to_new_file(file_path, dry_run=False):
    print(f'Will write "Hello world!" into {file_path}')
    if dry_run:
        print('Dry run, nothing changed')
    else:
        with file_path.open('w') as f:
            f.write('Hello world!\n')


def write_to_existing_file(file_path, dry_run=False):
    with file_path.open('r+') as f:
        if 'Hello world!\n' not in f.readlines():
            print(f'Will write "Hello world!" into {file_path}')
            if dry_run:
                print('Dry run, nothing changed')
            else:
                f.write('Hello world!\n')
        else:
            print(f'A line "Hello world!" already exists in {file_path}')


def main():
    parser = ArgumentParser('hello_world')
    parser.add_argument('file', help='File to manipulate')
    parser.add_argument('--dry-run', help='Dry run and check the result', action='store_true')
    args = parser.parse_args()
    write_hello_world(args)


if __name__ == '__main__':
    main()
