from setuptools import setup

setup(
    name='hello-world',
    version='0.1.0',
    packages=['hello_world'],
    entry_points={
        'console_scripts': [
            'hello-world=hello_world.cli:main',
        ],
    }
)
