import os
import sys

file_path = sys.argv[1]
if not os.path.exists(file_path):
    with open(file_path, 'w') as f:
        f.write('Hello world!\n')
else:
    with open(file_path, 'r+') as f:
        if 'Hello world!\n' not in f.readlines():
            f.write('Hello world!\n')
