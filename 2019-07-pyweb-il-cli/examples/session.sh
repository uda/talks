# Recorded with the doitlive recorder
#doitlive shell: /bin/zsh
#doitlive prompt: default

python hello_world_1.py hello_world_1.txt

cat hello_world_1.txt

python hello_world_1.py hello_world_1.txt

cat hello_world_1.txt


python hello_world_2.py -h

python hello_world_2.py hello_world_2.txt --dry-run

cat hello_world_2.txt

python hello_world_2.py hello_world_2.txt

cat hello_world_2.txt

python hello_world_2.py hello_world_2.txt

cat hello_world_2.txt


cd hello-world


python -m hello_world -h

python -m hello_world hello_world_3.txt --dry-run

cat hello_world_3.txt

python -m hello_world hello_world_3.txt

cat hello_world_3.txt


pip install .

hello-world -h

hello-world hello_world_4.txt --dry-run

cat hello_world_4.txt

hello-world hello_world_4.txt

cat hello_world_4.txt
