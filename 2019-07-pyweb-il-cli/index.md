title: Your command is my code
author: Yehuda Deutsch
event: PyWeb-IL #82
class: animation-fade
layout: true

.bottom-bar[
  {{title}} - {{author}} @ {{event}}
]

---

class: impact

# {{title}}
## Command line in python

---

# Hello world

.row[
.col-6[

```bash
$ echo Hello world!
Hello world!
```

```bash
$ echo Hello ${USER}!
Hello uda!
```

]
.col-6[

```python
>>> print('Hello world!')
Hello world!
```

```python
>>> import os
>>> user = os.getenv('USER')
>>> print(f'Hello {user}')
Hello uda!
```

]
]

---

# Put the genie in a bottle

.row[
.col-6[

```bash
#!/bin/bash
# hello_world.sh

echo Hello world!
```

```bash
$ bash hello_world.sh
Hello world!
```

```bash
$ chmod +x hello_world.sh
$ ./hello_world.sh
Hello world!
```

]
.col-6[

```python
#!/usr/bin/env python
# hello_world.py

print('Hello world!')
```

```bash
$ python hello_world.py
Hello world!
```

```bash
$ chmod +x hello_world.py
$ ./hello_world.py
Hello world!
```

]
]

---

# $ tar xvf presentation.tar.bz2

.center[
![Tar](tar.png)

I don't know what's worse--the fact that after 15 years of using tar I still can't keep the flags straight, or that after 15 years of technological advancement I'm still mucking with tar flags that were 15 years old when I started.

_[xkcd.com/1168](https://www.xkcd.com/1168/)_
]

---

# About

## Yehuda Deutsch

* Python developer
* Backand dev. and DevOps at [Zencity](https://zencity.io)


* uda @ [GitLab](https://gitlab.com/uda) / [GitHub](https://github.com/uda) / [Bitbucket](https://bitbucket.org/uda)
* ytd @ [Twitter](https://twitter.com/ytd)
* [yeh@uda.co.il](mailto:yeh@uda.co.il)

---

# Let's build a command line program

* Accept arguments and flags
* Create a file if it doesn't exist
* Make sure "Hello world!" exists in the file
* Allow dry run
* Run from anywhere
* Be aware from where it is run

---

# Simple

```python
import os
import sys

file_path = sys.argv[1]
if not os.path.exists(file_path):
    with open(file_path, 'w') as f:
        f.write('Hello world!\n')
else:
    with open(file_path, 'r+') as f:
        if 'Hello world!\n' not in f.readlines():
            f.write('Hello world!\n')
```

---

# Structured (1)

```python
def write_hello_world(args): pass

if __name__ == '__main__':
    from argparse import ArgumentParser
    parser = ArgumentParser('hello_world')
    parser.add_argument('file', help='File to manipulate')
    parser.add_argument('--dry-run',
                        help='Dry run and check the result',
                        action='store_true')
    args = parser.parse_args()
    write_hello_world(args)
```

---

# Structured (2)

```python
import os
from pathlib import Path
def write_to_new_file(file_path, dry_run=False): pass
def write_to_existing_file(file_path, dry_run=False): pass

def write_hello_world(args):
    cwd = Path(os.getenv('PWD'))
    file_path = cwd.joinpath(args.file)
    if file_path.exists():
        write_to_existing_file(file_path, dry_run=args.dry_run)
    else:
        write_to_new_file(file_path, dry_run=args.dry_run)
```

---

# Structured (3)

```python
def write_to_new_file(file_path, dry_run=False):
    if dry_run:
        print('Dry run, nothing changed')
    else:
        with file_path.open('w') as f:
            f.write('Hello world!\n')

def write_to_existing_file(file_path, dry_run=False):
    with file_path.open('r+') as f:
        if 'Hello world!\n' not in f.readlines():
            if dry_run:
                print('Dry run, nothing changed')
            else:
                f.write('Hello world!\n')
```

---

# Let's wrap it (1)

```python
from argparse import ArgumentParser

def write_hello_world(args): pass

def main():
    parser = ArgumentParser('hello_world')
    parser.add_argument('file', help='File to manipulate')
    parser.add_argument('--dry-run',
                        help='Dry run and check the result',
                        action='store_true')
    args = parser.parse_args()
    write_hello_world(args)

if __name__ == '__main__':
    main()
```

---

# Run it (1)

```bash
$ python hello_world.py hello_world.txt
$ cat hello_world.txt
Hello world!
```

--

```python
from hello_world import main

main()
```

---

# Let's wrap it (2a)

```python
# hello-world/hello_world/__main__.py
from .hello_world import main

main()
```

---

# Let's wrap it (2b)

```python
# hello-world/setup.py
from setuptools import setup

setup(
    name='hello-world',
    version='0.1.0',
    packages=['hello_world'],
    entry_points={
        'console_scripts': [
            'hello-world=hello_world.hello_world:main',
        ],
    }
)
```

---

# Let's run it (2)

```bash
$ python -m hello_world hello_world.txt
$ cat hello_world.txt
Hello world!
```

--

```bash
$ pip install .
$ hello-world hello_world.txt
$ cat hello_world.txt
Hello world!
```

---

# Advanced

## CLI frameworks

* [Click](https://click.palletsprojects.com/)
* [Invoke](https://docs.pyinvoke.org/)
* [docopt](http://docopt.org/)

## A great read:

[Comparing Python Command-Line Parsing Libraries](https://realpython.com/comparing-python-command-line-parsing-libraries-argparse-docopt-click/)

---

class: impact

# Questions?

---

class: impact

# Thank you!
