# FastAPI, Django Ninja and demoed implementing simple API

## Projects and Libraries

FastAPI - https://fastapi.tiangolo.com/

A simple web service framework, focused on async support, can be used for quick and simple solutions, very useful for micro-service designed systems. Amongst other features, provides API documentation in the widely implemented Swagger / OpenAPI and in ReDoc

Django Ninja - https://django-ninja.rest-framework.com/

A simple tap into the Django utilities while providing a programming interface based on FastAPI and similar to most micro-web-frameworks, while having all Django built-ins at your disposal, very useful for existing projects wanting to add documented API endpoints

Pydantic - https://docs.pydantic.dev/

A powerful yet simple structured data mechanism used for data validation and settings management, this provides two crucial features in FastAPI, the input representation / validation and settings management

OpenAPI / Swagger - https://swagger.io/specification/

My usage of Swagger for exploring Kubernetes' version APIs - https://uda.gitlab.io/kubernetes-swagger/

RequestBin / Pipedream - https://pipedream.com/

Formerly Known As RequestBin, very useful for investigating requests and test different responses
(The current usage is focused on automating and connecting systems, like automatically posting to Mastodon from an RSS... but I still use it for HTTP request investigations sometimes)

Python PyPI simple API specification - https://peps.python.org/pep-0691/

Refers also to previous specifications that impact the potential usage and implementation

## Implementations

Simple API proxy implemented in FastAPI - https://gitlab.com/uda/fast-pypi

Simple API hosting implemented in Django + DjangoNinja - https://gitlab.com/uda/ninja-pypi
