#!/bin/bash

bs_cmd="${PWD}/node_modules/backslide/bin/bs"
public_dir="${PWD}/public"

mkdir -p "${public_dir}"

known_dirs=(
    examples
)

backslide_talks=(
    2019-07-pyweb-il-cli
)

for talk in "${backslide_talks[@]}"; do
    echo Exporting $talk
    mkdir -p "${public_dir}/${talk}"
    (cd "$talk" && $bs_cmd export)
    cp ${talk}/dist/*.html "${public_dir}/${talk}/"
    if [ ! -d "${public_dir}/$talk" ]; then
        echo 'Failed copying talk' $talk
        exit
    fi
    for dir in "${known_dirs}"; do
        sub_dir="${talk}/${dir}"
        if [ -d "$sub_dir" ]; then
            cp -r "$sub_dir" "${public_dir}/$talk"
        fi
    done
done

